# M4 HTML

##Pasos per configurar el GIT

1. Crear usuari a GitLab
2. Crear projecte M4 HTML
3. Crear usuari Linux.
4. Configurar GIT:

git --config global user.email tonimejiasiese1b@gmail.com
git --config global user.name Toni

5. Crear una clau criptogràfica
----
ssh-keygen -t rsa -C "tonimejiasiese1b@gmail.com" -b 4096
----

6. Pujar clau criptogràfica al GitLab:

- Perfil de l'usuari
- Claus ssh
- Enganxar clau pública (ssh-keygen)
